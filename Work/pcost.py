# pcost.py
#
# Exercise 1.27

import os
import sys

def portfolio_cost(filename):
    f = open(filename,'rt') #open a file
    headers = next(f) # get header string
    total_cost = 0;
    for lines in f:
        mylinearray = lines.split(',')
        try:
            share_number = float(mylinearray[1])
        except ValueError:
            print("Couldn't parse file",line)
        share_price = float(mylinearray[2])
        total_price_line = share_number * share_price
        total_cost = total_cost + total_price_line
        print(lines, total_price_line)
    return total_cost


if len(sys.argv) == 2: #sys is a list that contains pased arguments on the cmmand line ( if any). run from terminal
    filename = sys.argv[1]
else:
    filename = 'Data/portfolio.csv'


cost = portfolio_cost('Data/portfolio.csv')
print('Total Cost in portfolio:',cost)
