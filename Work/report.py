# report.py
#
# Exercise 2.4
import csv
import os

def read_portfolio(filename):

    portfolio = []
    with open(filename,'rt') as f:
        rows = csv.reader(f)
        headers = next(rows)
        for row in rows: #turn row into a tuple
            holding = (row[0], int(row[1]), float(row[2]))
            portfolio.append(holding) # append empty list with the made tuple -> list of tuples
    return portfolio


def read_csv(filename):
    with open(filename) as f:
        lines = f.read().split("\n")
    return [line.split(",") for line in lines]

def read_portfolio_dict(filename):

    portfolio = []
    with open(filename,'rt') as f:
        rows = csv.reader(f)
        headers = next(rows)
        for row in rows: #turn row into a tuple
            stock = {
                "name":row[0],
                "shares":int(row[1]),
                "price" : float(row[2])
            }
            portfolio.append(stock)
    return portfolio

def read_prices(filename):
    prices = {}
    with open(filename,'rt') as f:
        rows = csv.reader(f)
        headers = next(rows)
        for row in rows: #turn row into a tuple
            prices[row[0]] = float(row[1])
    return prices



portfolio = read_portfolio_dict('Data/portfolio.csv')
prices = read_prices('Data/prices.csv')

# make report from prices and read_portfolio
def make_report(prices, portfolio):

    report_lines = []
    print(portfolio)
    for stocks in portfolio: #iterate over dicionry keys
        name = stocks
        shares = stocks['shares']
        price = stocks['price']
        market_price = prices[stocks['name']]
        change_price = market_price - price
        report_row = (name, shares, price, change_price)
        report_lines = report_lines.append(report_row)
    return report_lines


# read in file and report Data



# call function into report rows
report = make_report(portfolio,prices)

# Output the report
headers = ('Name', 'Shares', 'Price', 'Change')

print('%10s %10s %10s %10s' % headers) #?

print(('-' * 10 + ' ') * len(headers)) #?

for row in report:
    print('%10s %10d %10.2f %10.2f' % row) #
